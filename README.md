Incredibly minimal "kernel" and "libc", based on http://wiki.osdev.org/User:Sortie/Meaty_Skeleton

To build:


```
#!

cd libc
make install-headers
cd ../kernel 
make install-headers
cd ../libc
make
make install
cd ../kernel
make

qemu-system-x86_64 myos.bin
```

Tested with GCC 4.8.2 / Binutils 2.24