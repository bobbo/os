#include <string.h>

void* memcpy(void* __restrict buf1, const void* __restrict buf2, size_t size)
{
	register char *reg1 = buf1;
	register const char *reg2 = buf2;

	while (size)
	{
		*reg1++ = *reg2++;
		--size;
	}

	return buf1;
}