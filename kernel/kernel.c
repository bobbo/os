#include <stdio.h>
#include <string.h>
#include <kernel/tty.h>
#include "arch/i386/pmode.h"

void kernel_early()
{
	tty_initialize();
	printf("%s\n", "David's Not-really-an-OS v0.00000.01~pre-alpha");
	
}

void kernel_main()
{
	enter_protected_mode();
}