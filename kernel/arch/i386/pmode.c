#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "isr.h"

#define GDT_NUM_ENTRIES 5
#define IDT_NUM_ENTRIES 256

typedef struct __attribute__((packed)) {
	uint16_t limit_low;
	uint16_t base_low;
	uint8_t base_middle;
	uint8_t access;
	uint8_t granularity;
	uint8_t base_high;
} gdt_entry_t;

typedef struct __attribute__((packed)) {
	uint16_t limit;
	uint32_t base;
} gdt_t;

gdt_entry_t gdt_entries[GDT_NUM_ENTRIES];
gdt_t gdt;

typedef struct __attribute__((packed)) {
	uint16_t base_low;
	uint16_t seg_selector;
	uint8_t always_zero;
	uint8_t flags;
	uint16_t base_high;
} idt_entry_t;

typedef struct __attribute__((packed)) {
	uint16_t limit;
	uint32_t base;
} idt_t;

idt_entry_t idt_entries[IDT_NUM_ENTRIES];
idt_t idt;

extern void gdt_flush(uint32_t);
extern void idt_flush(uint32_t);
extern void pmode_switch();

static void outb(uint8_t value, uint16_t port)
{
	asm volatile("outb %0,%1" : : "a" (value), "dN" (port));
}

static void gdt_set_gate(int32_t num, uint32_t base, uint32_t limit, uint8_t access, uint8_t granularity)
{
	gdt_entry_t entry = gdt_entries[num];

	entry.base_low = (base & 0xFFFF);
	entry.base_middle = (base >> 16) & 0xFF;
	entry.base_middle = (base >> 24) & 0xFF;

	entry.limit_low = (limit & 0xFFFF);
	entry.granularity = (limit >> 16) & 0x0F;

	entry.granularity |= granularity & 0xF0;
	entry.access = access;
}

static void gdt_init()
{
	gdt.limit = (sizeof(gdt_entry_t) * GDT_NUM_ENTRIES) - 1;
	gdt.base = (uint32_t) &gdt_entries;

	gdt_set_gate(0, 0, 0, 0, 0);
	gdt_set_gate(1, 0, 0xFFFFFFFF, 0x9A, 0xCF);
	gdt_set_gate(2, 0, 0xFFFFFFFF, 0x92, 0xCF);
	gdt_set_gate(3, 0, 0xFFFFFFFF, 0xFA, 0xCF);
	gdt_set_gate(4, 0, 0xFFFFFFFF, 0xF2, 0xCF);
}

static void idt_set_gate(uint8_t num, uint32_t base, uint16_t selector, uint8_t flags)
{
	idt_entry_t entry = idt_entries[num];

	entry.base_low = base & 0xFFFF;
	entry.base_high = (base >> 16) & 0xFFFF;

	entry.seg_selector = selector;
	entry.always_zero = 0;

	entry.flags = flags; /* | 0x60 // For user-mode */
}

static void idt_init()
{
	idt.limit = (sizeof(idt_entry_t) * IDT_NUM_ENTRIES) - 1;
	idt.base = (uint32_t) &idt_entries;

	memset(&idt_entries, 0, sizeof(idt_entry_t) * IDT_NUM_ENTRIES);

	idt_set_gate(0, (uint32_t) isr_0, 0x08, 0x8E);
	idt_set_gate(1, (uint32_t) isr_1, 0x08, 0x8E);
	idt_set_gate(2, (uint32_t) isr_2, 0x08, 0x8E);
	idt_set_gate(3, (uint32_t) isr_3, 0x08, 0x8E);
	idt_set_gate(4, (uint32_t) isr_4, 0x08, 0x8E);
	idt_set_gate(5, (uint32_t) isr_5, 0x08, 0x8E);
	idt_set_gate(6, (uint32_t) isr_6, 0x08, 0x8E);
	idt_set_gate(7, (uint32_t) isr_7, 0x08, 0x8E);
	idt_set_gate(8, (uint32_t) isr_8, 0x08, 0x8E);
	idt_set_gate(9, (uint32_t) isr_9, 0x08, 0x8E);
	idt_set_gate(10, (uint32_t) isr_10, 0x08, 0x8E);
	idt_set_gate(11, (uint32_t) isr_11, 0x08, 0x8E);
	idt_set_gate(12, (uint32_t) isr_12, 0x08, 0x8E);
	idt_set_gate(13, (uint32_t) isr_13, 0x08, 0x8E);
	idt_set_gate(14, (uint32_t) isr_14, 0x08, 0x8E);
	idt_set_gate(15, (uint32_t) isr_15, 0x08, 0x8E);
	idt_set_gate(16, (uint32_t) isr_16, 0x08, 0x8E);
	idt_set_gate(17, (uint32_t) isr_17, 0x08, 0x8E);
	idt_set_gate(18, (uint32_t) isr_18, 0x08, 0x8E);
	idt_set_gate(19, (uint32_t) isr_19, 0x08, 0x8E);
	idt_set_gate(20, (uint32_t) isr_20, 0x08, 0x8E);
	idt_set_gate(21, (uint32_t) isr_21, 0x08, 0x8E);
	idt_set_gate(22, (uint32_t) isr_22, 0x08, 0x8E);
	idt_set_gate(23, (uint32_t) isr_23, 0x08, 0x8E);
	idt_set_gate(24, (uint32_t) isr_24, 0x08, 0x8E);
	idt_set_gate(25, (uint32_t) isr_25, 0x08, 0x8E);
	idt_set_gate(26, (uint32_t) isr_26, 0x08, 0x8E);
	idt_set_gate(27, (uint32_t) isr_27, 0x08, 0x8E);
	idt_set_gate(28, (uint32_t) isr_28, 0x08, 0x8E);
	idt_set_gate(29, (uint32_t) isr_29, 0x08, 0x8E);
	idt_set_gate(30, (uint32_t) isr_30, 0x08, 0x8E);
	idt_set_gate(31, (uint32_t) isr_31, 0x08, 0x8E);

	idt_flush((uint32_t) &idt);
}

void enter_protected_mode()
{
	/* Disable interrupts */
	asm volatile("cli");

	/* Disable NMI */
	outb(0x80, 0x70);
	// Some machines might need a delaying dummy write to port 0x80

	gdt_init();

#ifdef __DEBUG
	printf("GDT loaded\n");
#endif

	idt_init();

#ifdef __DEBUG
	printf("IDT loaded\n");
#endif
	//pmode_switch();
}