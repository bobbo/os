.global gdt_flush
gdt_flush:
   movl 4(%esp), %eax
   lgdt (%eax)

   mov $0x10, %ax
   mov %ax, %ds
   mov %ax, %es
   mov %ax, %fs
   mov %ax, %gs
   mov %ax, %ss
   jmp _flush
   # GDT examples use a 0x08:_flush jmp, think above jmp is wrong

_flush:
   ret

.global idt_flush
idt_flush:
   mov 4(%esp), %eax 
   lidt (%eax)       
   ret