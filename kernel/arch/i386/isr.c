#include <stdio.h>

#include "isr.h"

void isr_handler(registers_t registers)
{
	printf("Received interrupt: \n");
	printf("%c\n", (char)(((int)'0') + registers.int_no));
}