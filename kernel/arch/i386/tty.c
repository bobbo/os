#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
 
#include <kernel/vga.h>
#include <kernel/tty.h>

size_t tty_row;
size_t tty_column;
uint8_t tty_color;
uint16_t tty_buffer[VGA_HEIGHT * VGA_WIDTH];
uint16_t* tty_vmem;
 
void tty_initialize()
{
	tty_row = 0;
	tty_column = 0;
	tty_color = make_color(COLOR_LIGHT_GREY, COLOR_BLACK);
	tty_vmem = VGA_MEMORY;
	for ( size_t y = 0; y < VGA_HEIGHT; y++ )
	{
		for ( size_t x = 0; x < VGA_WIDTH; x++ )
		{
			const size_t index = y * VGA_WIDTH + x;
			tty_buffer[index] = make_vgaentry(' ', tty_color);
		}
	}

	tty_flush();
}
 
void tty_flush()
{
	memcpy(tty_vmem, tty_buffer, VGA_WIDTH * VGA_HEIGHT * 2);
}

void tty_setcolor(uint8_t color)
{
	tty_color = color;
}
 
/* Potentially common between architectures if needed */
int tty_is_common_char(char c)
{
	switch (c) {
		case TTY_NEWLINE:
			return 0;
	}

	return 1;
}

void tty_putentryat(char c, uint8_t color, size_t x, size_t y)
{
	const size_t index = y * VGA_WIDTH + x;
	tty_buffer[index] = make_vgaentry(c, color);
}
 
void tty_putchar(char c)
{
	if (tty_is_common_char(c))
	{
		tty_putentryat(c, tty_color, tty_column, tty_row);

		if (++tty_column == VGA_WIDTH)
			tty_column = 0;
	}
	else
	{
		if (c == TTY_NEWLINE) 
		{
			tty_row++;
			tty_column = 0;
		}
	}

	if (tty_row == VGA_HEIGHT) 
	{
		for (unsigned int i = 1; i < VGA_HEIGHT; i++)
		{
			memcpy(tty_buffer + ((i - 1) * VGA_WIDTH), tty_buffer + (i * VGA_WIDTH), VGA_WIDTH * 2);
		}

		tty_column = 0;
		tty_row--;
	}
}
 
void tty_write(const char* data, size_t size)
{
	for ( size_t i = 0; i < size; i++ )
		tty_putchar(data[i]);

	tty_flush();
}
 
void tty_writestring(const char* data)
{
	tty_write(data, strlen(data));
}


